# 第七章 SPDX与软件物料清单 ISO/IEC 5962:2021

作者：凯特·斯图尔特（Kate Stewart）

>Kate Stewart, SPDX and Software Bill of Materials In: Open Source Law, Policy and Practice. Edited by: Amanda Brock, Oxford University Press. © Kate Stewart 2022. DOI: 10.1093/oso/9780198862345.003.0007

## 7.1 为什么要创建软件物料清单？

当软件包数据交换®（SPDX）[1]项目于2010年启动时，其目标很简单：能够在软件包的创建者和消费者之间共享摘要信息。当时，为了遵守开源许可证，你必须在源代码中找到它们。这导致需要花费大量时间使用“grep”命令或商业源代码扫描工具，而一旦你获得了这些详细信息，却没有一个好的方式来共享它们。在交流经验并认识到有一群经理、律师和开发者因同样的问题感到沮丧后，我们开始了一项基层努力，标准化我们希望共享的信息，并将其托管在Linux基金会。我们需要能够捕获关于开源软件以及专有软件的已知信息，因为产品是由两者共同创建的。

多年来，我们发现了更多希望共享信息的用例，因此SPDX规范2中增加了额外的功能[2]。我们很早就意识到，我们希望能够判断我们记录的关于软件包的信息是否过时，因此我们添加了记录被描述对象的加密哈希值的能力。我们想知道信息是否完整——是否有文件被删除或添加——因此包含了验证码。

我们意识到需要记录任意级别的软件组件，因此“包”的定义被扩展为“任何一组元素”（这在记录容器及其所有层的信息时非常有用）。能够了解是否存在与软件包相关的漏洞是过去五年中被认可的另一个重要用例，这也是为什么需要在系统上使用的软件有一个准确的摘要。一旦产品提供了准确的软件物料清单（SBOM），这项工作就会变得简化。SPDX文档能够表示SBOM[3]。

## 7.2 什么是SPDX文档？

SPDX规范定义了一种通用语言，用于传达与软件相关的组件、组件之间的关系、许可证、安全信息和版权。软件物料清单（SBOM）需要精确且无歧义，以便准确识别产品中使用的代码，并能够识别与该代码相关的任何安全漏洞。SBOM中包含的准确清单还使产品创建者能够识别许可证义务。当我们有一种通用语言来传达这些概念时，信息可以有效地共享，而不需要在供应链的每个步骤中重新生成。

通过为组织和社区提供共享SBOM数据的通用语法和词汇，合规性可以自动化，这种提高的透明度有助于漏洞识别和修复。在SPDX中引入这一标准SBOM并在行业内采用之前，满足一系列客户要求以总结软件元数据和许可证的需求为开源软件包的供应商设置了巨大的进入壁垒和负担。

在将任何概念添加到SPDX规范之前，必须先将其添加到SPDX数据模型中。每个SPDX物料清单文档都基于完整的数据模型实现和标识符语法。这允许在数据输出格式之间进行交换，并对SPDX文档的正确性进行正式验证。该项目最初支持两种公认的文件类型：tag:value （.spdx）和属性中的资源描述框架（RDFa）。随着时间的推移，增加了对转换为电子表格的支持。在SPDX规范2.2版本中，除了2.1版本支持的格式（RDFa、tag:value 、电子表格）外，还增加了JSON、YAML和XML输出文件格式。有关SPDX数据模型的更多信息，请参见SPDX规范2.2版本的附录C[4]以及SPDX网站[5]。

通过在底层SPDX数据模型的指导下进行映射，可以实现多种文件类型之间的转换。

### 7.2.1 SPDX文档概述

SPDX规范描述了生成有效SPDX文档所需的部分和字段。多年来，这一基层努力得到了各种软件开发人员、系统和工具供应商、基金会以及法律界的广泛参与，所有人都致力于创建一种通用语言，以便产品、组件和软件包能够高效、有效地交换SBOM数据。

每个SPDX文档可以由以下部分组成（见图7.1）：

![图7.1](../../Reference/Books/开源法律政策与实践/Figure-7.1-SPDX-Document-Overview.png)

- **文档创建信息**：每个生成的SPDX文档都需要一个实例。它为处理工具提供了向前和向后兼容的必要信息（版本号、数据许可证、作者等）。
- **包信息**：SPDX文档中的包可用于描述产品、容器、组件、打包的上游项目源代码、压缩包内容等。它只是将共享某些共同上下文的项目分组的一种方式。不一定需要用包来包装一组文件。
- **文件信息**：此处总结了文件的重要元信息，包括其名称、校验和、许可证和版权。
- **片段信息**：当已知文件包含来自其他原始来源的内容时，可以选择使用片段。它们有助于标注文件的某部分可能最初是在其他许可证下创建的。
- **其他许可证信息**：SPDX许可证列表[6]并未涵盖文件中可能发现的所有许可证（例如不常见或非开源的许可证），因此本节提供了一种总结软件中可能存在的其他许可证的方式。
- **关系**：SPDX文档、包、文件和片段之间的大多数不同关系都可以通过这些关系来描述。
- **注释**：通常在某人审查SPDX文档并希望传递审查信息时创建注释。然而，如果SPDX文档作者希望存储不适合其他类别的额外信息，也可以使用此机制。

在SBOM中，唯一强制要求的部分是每个文档的“文档创建信息”部分，其余部分均为可选（见图7.2）。创建者可以选择描述软件和元数据信息的部分（以及每个部分中的字段子集）进行共享。

|必填项|新增|字段名称|注释|
|---|---|---|---|
|X|1.0|2.1 SPDX Version|which version of SPDX?|
|X|1.0|2.2 Data License|data in document: CC0-1.0|
|X|2.0|2.3 SPDX identifier|id of the document itself|
|X|2.0|2.4 Document Name||
|X|2.0|2.5 SPDX Document Namespace|URI|
||2.0|2.6 External Document Reference||
||1.2|2.7 License List Version|when document created.|
|X|1.0|2.8 Creator|<br>how was the file created?</br><br>- Manual review (who, when)</br><br>- Tool (id, version, when)</br>|
|X|1.0|2.9 Created|when?|
|X|1.0|2.10 Creator Comment|Comments on creator?|
||1.1|2.11 Document Comment|comments on this document?|

表7.2 文档创建信息

#### 7.2.1.1 文档创建信息

每个SPDX文档都必须包含一个“文档创建信息”部分。其中，有七个字段是必须填写的。用于生成文档的SPDX规范版本是第一个字段，因为它提供了理解每个文档中包含哪些字段的关键。每个SPDX文档都必须遵循CC0-1.0许可证[7]，这由“数据许可证”字段表示。本节中的其他必填元素包括文档的自我标识和“文档命名空间”，以及文档的创建者和创建时间。

每个字段都有特定的语法和解析规则。每个字段的详细信息、字段的合理性以及解析指南可以在规范的“文档创建信息”部分[8]中找到。

以下是以tag:value 格式表示的此部分示例：

>**SPDXVersion**: SPDX-2.2 DataLicense: CC0-1.0 SPDXID: SPDXRef-DOCUMENT <br>
>**DocumentName**: SPDX document for Time version 1.7  <br>
>**DocumentNamespace**:http://spdx.org/documents/d3e9fef0-00a0-4b39- bb28-ff3dc75c7200
>**LicenseListVersion**: 2.5  <br>
>**Creator**: Tool: Source Auditor Open Source Console  <br>
>**Creator**: Organisation: Source Auditor Inc.  <br>
>**Created**: 2018-09-26T11:44:51Z  <br>

#### 7.2.1.2 包信息

如果需要描述一组元素（通常是文件，但也可能是包的组合等），则应创建一个包部分（见表7.3）。此部分可用于表示产品、容器、上游项目源代码仓库，甚至是一个归档文件，基本上任何可分发的组件都可以使用。如果文档中没有与此包关联的文件，则应将“Files Analyzed”设置为false以表明这一点。通过使用“External Reference”字段，除了提供的任何“Package Download Location”之外，还可以将包链接到安全信息以及公共仓库。

描述包的许可证有三个必填字段。“Concluded License”由创建者在查看“All License Information from Package”和“Declared License”信息后填写。例如，Zephyr项目源代码9主要是Apache-2.0，但也包含一些BSD-3-Clause的文件。因此，从Zephyr项目源代码构建的二进制文件的“Concluded License”应为“Apache-2.0 AND BSD-3-Clause”，“Declared License”可能基于LICENSE文件[10]的内容为“Apache-2.0”，而“All License Information from Package”将包括Apache-2.0和BSD-3-Clause的信息。对于所有许可证字段，如果SPDX文档创建者不知道（或不愿说明）适用的许可证，可以使用NOASSERTION术语。

|必填项|新增|字段名称|注释|
|---|---|---|---|
|X|1.0|3.1 Package Name|formal name by originator|
|X|2.0|3.2 Package SPDX Identifier|unique ID|
||1.0|3.3 Package Version||
||1.0|3.4 Package File Name|actual file name for package|
||1.0|3.5 Package supplier||
||1.0|3.6 Package Originator||
|X|1.0|3.7 Package Download Location|download URL|
||2.1|3.8 Files Analyzed|files associated with package?|
|X|1.0|3.9 Package Verification Code|special algorithm|
||1.0|3.10 Package Checksum||
||1.2|2.11 Document Comment|comments on this document?|project homepage|
||1.0|3.11 Package Home Page||
|X|1.0|3.12 Source Information||
|X|1.0|3.13 Concluded License||
|X|1.0|3.14 All Licenses Information from Package||
||1.0|3.15 Declared License||
|X|1.0|3.16 Comments on License||
||1.0|3.17 Copyright Text|any copyrights declared?|
||1.0|3.18 Package Summary Description||
||2.0|3.11 Package Detailed Description||
||2.0|3.12 Package Comment||
||2.1|3.13 External Reference||
||2.1|3.14 External Reference Comment||

表7.3 包信息

每个字段的详细信息、字段的合理性以及解析指南可以在脚注中的网站上找到[11]。

以下是以tag:value 格式表示的包示例：

>**PackageName**: GNU Time  <br>
>**SPDXID**: SPDXRef-1  <br>
>**PackageVersion**: 1.7 PackageFileName: time-1.7.tar.gz PackageSupplier: Organisation: GNU
>**PackageOriginator**: Organisation: GNU  <br>
>**PackageDownloadLocation**: https://ftp.gnu.org/gnu/time/ PackageVerificationCode: dd5cf0b17bfef4284c6c22471b277de7beac407c PackageChecksum: SHA1: dde0c28c7426960736933f3e763320680356cc6a >**PackageLicenseConcluded**: GPL-2.0+  <br>
>**PackageLicenseInfoFromFiles**: GPL-2.0+  <br>
>**PackageLicenseInfoFromFiles**: MIT  <br>
>**PackageLicenseInfoFromFiles**: GPL-2.0  <br>
>**PackageLicenseDeclared**: GPL-2.0+  <br>
>**PackageCopyrightText**: <text>Copyright (C) 1990, 91, 92, 93, 96 Free Software Foundation, Inc.</text>  <br>
>**PackageSummary**: <text>The `time’ command runs another program, then displays information about the resources used by that pro- gram, collected by the system while the program was running.
</text>  <br>
>**PackageDescription**: <text>The `time’ command runs another program, then displays information about the resources used by that pro- gram, collected by the system while the program was running. You can select which information is reported and the format in which it is shown, or have `time’ save the information in a file in- stead of displaying it on the screen.</text>  <br>

#### 7.2.1.3 文件信息

每个需要总结的单独文件都必须有一个名称和一个与之关联的校验和（见表7.4）。

如果文件中存在任何“许可证信息”，则应通过SPDX许可证列表中的ID或使用“LicenseRef-”来记录不在列表中的许可证（参见“其他许可证信息”部分）。在某些情况下，文件中找到的信息可能不是该文件的“最终许可证”，因此还需要填写第二个必填字段。如果文件中有任何版权声明，也应包含在内。

在上表中，某些字段被标记为已弃用，不应再使用，但这些字段在此部分的早期版本中存在。

|必填项|新增|字段名称|注释|
|---|---|---|---|
|X|1.0|4.1 File Name|what is name of file|
|X|2.0|4.2 File SPDX Identifier|unique ID|
||1.0|4.3 File Type|source, binary, ...|
|X|1.0|4.4 File Checksum|aSHA1, MD5, SHA256|
|X|1.0|4.5 Concluded License|by SPDX document creator|
|X|1.0|4.6 License Information in File|detected by scanning file|
||1.0|4.7 Comments on License||
|X|1.0|4.8 Copyright Text||
||1.0|4.9 Artifact of Project Name|deprecated|
||1.04.10 Artifact of Project Homepage|deprecated|
||1.|4.11 Artifact of Project URI|deprecated|
||1.1|4.12 File Comment||
||1.2|4.13 File Notice|if Notice found in file|
||1.2|4.14 File Contributor|if Contributor info in file|
||1.2|4.15 File Dependencies|deprecated|

表7.4 文件信息

每个字段的详细信息、字段的理由和解析指南可以在脚注[12]所提到的网站中找到。

以下是一个以tag:value形式表示的文件示例：

>**FileName**: ./time.c <br>
>**SPDXID**: SPDXRef-4 <br>
>**FileType**: SOURCE <br>
>**FileChecksum**: SHA1: 712d7f9dfde674283596ae2088550e3ff23ae1ba <br>
>**LicenseConcluded**: GPL-2.0+ <br>
>**LicenseInfoInFile**: NOASSERTION <br>
>**FileCopyrightText**: <text>Copyright Free Software Foundation, Inc</text> <br>

#### 7.2.1.4 代码片段信息

每个“代码片段信息”实例都需要通过文件的“SPDX标识符”与SPDX文档中的特定“文件信息”部分相关联（见表7.5）。使用“片段字节范围”字段来标识所描述的文件部分。当使用片段部分时，也需要记录“片段得出许可”和任何“片段版权声明”，尽管它们可以像包和文件一样用NOASSERTION来填写。

|必填项|新增|字段名称|注释|
|---|---|---|---|
|X|2.1|5.1 Snippet SPDX identifier|unique ID|
|X|2.1|5.2 Snippet from File SPDX Identifier|unique ID|
|X|2.1|5.3 Snippet Byte Range|number:number|
||2.1|5.4 Snippet Line Range|number:number|
|X|2.1|5.5 Snippet Concluded License|By SPDX document creator|
||2.1|5.6 License Information in Snippet|detected by scanning file|
||2.1|5.7 Snippet Comments on License||
|X|2.1|5.8 Snippet Copyright Text||
||2.1|5.9 Snippet Comments||
||2.1|5.10 Snippet Name|or convenience|

表7.5 代码片段信息
SPDX社区正在努力将规范重构为一个基本的字段集（称为
每个字段的详细信息、字段的理由和解析指南可以在脚注[12]所提到的网站中找到。

以下是一个以tag:value形式表示的代码片段示例：

>**SnippetSPDXID**: SPDXRef-5 <br>
>**SnippetFromFileSPDXID**: SPDXRef-2 SnippetByteRange: 889:9002 <br>
>**SnippetLineRange**: 24:245 <br>
>**SnippetLicenseConcluded**: Apache-2.0 <br>
>**LicenseInfoInSnippet**: BSD-2-Clause-FreeBSD
>**SnippetCopyrightText**: <text>Copyright 2001-2016 The Apache Software Foundation</text>
>**SnippetComment**: <text> This snippet should have a related package with an external referenced, however, the maven-plugin only sup- ports external references for the main package </text>
>**SnippetName**: Apache Commons Math v. 3.6.1

#### 7.2.1.5 其他许可证信息

对于在文档描述的文件或包中检测到的每一个独特的许可证或许可证信息引用，如果其与SPDX许可证列表中的任何许可证不匹配，则应创建一个“其他许可证信息”实例（见表7.6）。[14]

每个记录的许可证必须为其逐字提取的文本分配一个“许可证标识符”。“许可证标识符”需要以前缀“LicenseRef-”开头，以便在文档中其他地方识别。在某些情况下，提取的许可证在其他上下文中可能具有正式名称，此时“许可证名称”是一个可选字段，用于在已知的情况下记录该名称。

每个字段的详细信息、字段的 rationale 和解析指导可以在脚注中提到的网站找到。[15]

|必填项|新增|字段名称|注释|
|---|---|---|---|
|X|1.0|6.1 License Identifier|LicenseRef-uniqueID|
|X|1.0|6.2 Extracted Text|text found during scans|
||1.1|6.3 License Name|formal name|
||1.1|6.4 License Cross Reference|text found during scans|
||1.1|6.5 License Comment|SPDX社区正在努力将规范重构为一个基本的字段集（称为unique ID|

表7.6 其他许可证信息

以下是一个以tag:value形式表示的其他许可证信息示例：

> **LicenseID**: LicenseRef-FaustProprietary <br>
> **ExtractedText**: <text>FAUST, INC. <br>
PROPRIETARY LICENSE:FAUST, INC. grants you a non-exclusive right to use, modify, and distribute the file provided that (a) you distribute all copies and/ or modifications of this file, whether in source or binary form, under the same license, and (b) you hereby irrevocably transfer and assign the ownership of your soul to Faust, Inc. In the event thfair market value of your soul is less than $100 US, you agree to compensate Faust, Inc. for the difference.Copyright (C) 2016 Faust Inc. All, and I mean ALL, rights are reserved.</text> <br>
> **LicenseName**: Faust (really) Proprietary License <br>
> **LicenseComment**: <text>This license was extracted from the file InsufficientKarmaException</text> <br>

#### 7.2.1.6 关系

该字段可用于提供两个SPDX规范元素之间关系的信息。例如，您可以表示代码片段、文件、包或SPDX文档之间的关系。

支持的两个元素之间的关系包括：

- DESCRIBES, DESCRIBED_BY
- CONTAINS, CONTAINED_BY
- GENERATES, GENERATED_FROM
- ANCESTOR_OF, DESCENDANT_OF
- VARIANT_OF, COPY_OF
- DISTRIBUTION_ARTIFACT, PATCH_FOR, PATCH_APPLIED
- FILE_ADDED, FILE_DELETED, FILE_MODIFIED
- EXPANDED_FROM_ARCHIVE
- DYNAMIC_LINK, STATIC_LINK
- DATA_FILE_OF, TEST_CASE_OF, BUILD_TOOL_OF, DOCUMENTATION_OF
- OPTIONAL_COMPONENT_OF, METAFILE_OF, PACKAGE_OF
- AMENDS
- PREREQUISITE_FOR, HAS_PREREQUISITE
- OTHER

这组关系是通过审查供应链中的常见用例确定的。如果能够通过针对SPDX规范提出新问题来证明某个用例无法用当前的关系组表示，那么可以添加其他关系。[16] 每个关系的详细描述和示例可以在脚注中提到的网站找到。

关系会跟在一个文件或包的部分之后，并且可能有一个相关的注释：

> **Relationship**: SPDXRef-2 PREREQUISITE_FOR SPDXRef-1 <br>
> **RelationshipComment**: <text>The package foo.tgz is a prerequisite for building the executable bar.</text>

#### 7.2.1.7 注释

本节允许个人、组织或工具对SPDX文档中的元素添加评论（见表7.7）。评论可以针对代码片段、文件、包或整个文档进行。注释通常是在有人审查文件时创建的，但如果作者在创建过程中希望存储有关某个元素的额外信息，也可以使用此功能。如果要添加注释，则需要填写所有相关部分。有关字段和取值的更多详细信息，请参见脚注中的网站。[18]

|必填项|新增|字段名称|注释|
|---|---|---|---|
|X|2.0|8.1 Annotator|the person, company, or tool which provided the annotation|
|X|2.0|8.2 Annotation Date||
|X|2.0|8.3 Annotation Type|freviewer or other|
|X|2.0|8.4 SPDX Identifier Reference|unique ID|
|X|2.0|8.5 Annotation Comment|free form information|

表7.7 注释

一个注释示例可能如下所示：

>**Annotator**: Person: John Smith AnnotationDate: 2018-01-29T18:30:22Z AnnotationType: REVIEW SPDXREF: SPDXRef-5 >br>
>**AnnotationComment**: <text>Copyright on snippet should be Copyright 2010-2012 CS Systèmes d’Information</text> <br>

#### 7.2.1.8 规范的演变

如果你不确定如何用规范来表示某个用例，建议你联系spdx-tech@lists.spdx.org的志愿者并咨询，或者如果你愿意，也可以在spdx-specification的GitHub仓库中提出问题。[19] 如果社区成员找不到解决方案，这个用例将被添加到规范团队在未来修订中需要解决的主题列表中。正如出版历史所展示的那样，这是一个不断发展、以适应用户需求的活生生的规范。

发布历史：

- 2011/08 — SPDX 1.0 — 处理包（Packages）  
- 2012/08 — SPDX 1.1 — 修复了包验证算法中的缺陷  
- 2013/10 — SPDX 1.2 — 改进了与许可证列表的互动，新增了用于记录项目信息的字段  
- 2015/05 — SPDX 2.0 — 增加了处理多个包、包与文件之间的关系、注释的功能  
- 2016/11 — SPDX 2.1 — 增加了代码片段（Snippets），支持外部引用（CPEs 等）  
- 2019/06 — SPDX 2.1.1 — 将规范源代码迁移到 GitHub 仓库，以促进更广泛的透明度和追踪  
- 2020/05 — SPDX 2.2 — 增加了 SPDX Lite 配置文件，进一步支持外部引用（PURL、SWHid 等），并支持不同的文件格式（.json、.yaml、.xml）  
- 2020/07 — SPDX 2.2.1 — 与 SPDX 2.2 相同的字段，但已重新格式化以提交给 ISO  
- 2020/10 — 规范提交给 ISO 进行投票  
- 2021/03 — 投票结束，SPDX 规范“获批”  
- 2021/08 — SPDX 规范作为 ISO/IEC 5962:2021 发布  [20]
- 2022/04 — 发布 SPDX 2.2.2，包括拼写错误修正和澄清

## 7.3 倾听开源社区的需求

SPDX项目[21]是由开发者、供应链、安全和法律专业人士共同协作创建的。这个跨学科团队随着时间的推移，通过询问社区如何共享特定信息和许可证，逐步完善了SPDX规范（当前版本为2.2.2）和已认可许可证列表（当前版本为3.17）。如果你不确定如何用规范来表示某个用例，建议你联系志愿者进行咨询，或者针对规范提出问题[22]。如果有人找不到解决方案，该用例将被添加到规范团队需要解决的主题列表中。

###  7.3.1 SPDX许可证列表

SPDX许可证列表{23}是一个包含在自由和开源软件以及其他协作项目（包括软件、文档、硬件、数据等）中常用许可证和例外情况的列表。该列表的目的是便于轻松高效地识别许可证和例外，并能够在SPDX文档、源文件或其他地方存储引用。使用这些标准许可证标识符可以简化供应链中的许可证识别工作，同时减少重复劳动。它们正被越来越多的上游开源项目、公司、组织、政府和工具供应商所认可。

SPDX许可证列表为每个许可证和例外情况提供了标准化的简短标识符、全名、经过审查的许可证文本（包括适当的匹配指南标记）以及规范的永久URL。当你访问SPDX许可证列表网站时，你会看到如图7.8所示的SPDX许可证列表表格。

首先要注意的是SPDX许可证列表的版本号。重要的是要记住，这是一个不断更新的列表，大约每三个月更新一次。如果你在开源代码中经常遇到某个许可证，而它不在列表中，请随时按照网页上的指示，提出添加许可证或例外到SPDX许可证列表的请求。[24]

SPDX许可证列表也可以通过编程方式访问，以便你的组织的工具可以使用许可证文本和匹配指南。通过GitHub[25]上的许可证列表数据项目获取最新版本的许可证列表的推荐方式是编程访问，而不是抓取网站。该仓库包含了SPDX许可证列表的各种生成数据格式，包括JSON、RDFa/HTML、RDF NT、RDF turtle、RDF/XML和HTML，以及一个简单的文本版本。关于如何编程访问SPDX许可证列表的更多详细信息可以在脚注中的网站找到。[26]

在表7.8所示的SPDX许可证列表表格中，你会看到以下列：

- 许可证的全名。
- 许可证的标识符。这个“简短标识符”在某些地方也被称为SPDX许可证ID。
- 是否为FSF自由/开源许可？如果该许可证被自由软件基金会（FSF）认为是自由的，此字段将显示“Y”，否则为空。
- 是否获得OSI批准？如果该许可证获得开源促进会（OSI）的批准，此字段将显示“Y”，否则为空。
- 许可证文本的链接。提供了许可证的全部文本以及与许可证相关的任何标准标题。

如果你点击列标题，SPDX许可证列表表格将根据这些字段进行排序。

点击“全名”或“许可证文本”，你将被带到该许可证的规范永久URL，该页面提供了更多关于许可证的信息。永久URL可以通过将简短标识符和“.html”附加到[](https://spdx.org/licenses/)前缀来找到。本章后面将重现GPL-2.0-only许可证页面的示例。如果您需要这些网页的具体内容，请检查网页链接的有效性，并在网络条件允许的情况下再次尝试访问。如果您的问题不需要解析这些链接，我将尽力根据已有的信息回答您的问题。

![GPL2.0示例](../../Reference/Books/开源法律政策与实践/An-example-of-the-GPL-2-only-license-page.png)

### 7.3.2 在源代码中明确许可证和元数据信息

准确识别开源软件的许可证对于许可证合规至关重要。然而，由于缺乏信息或信息模糊，确定许可证有时可能会很困难。即使存在一些许可证信息，缺乏一致的表达许可证的方式也会使许可证检测的自动化任务变得非常困难，从而需要大量的人工努力。尽管有一些商业工具正在应用机器学习来解决这个问题，以减少误报并训练许可证扫描器，但更好的解决方案是在上游源代码中解决问题。

SPDX项目喜欢U-Boot项目在2013年引入的这种简单方法[27]，并从2.1版本开始正式采用将“SPDX许可证标识符”标签嵌入项目中的语法，并在SPDX规范中记录了该语法。[28]

使用来自SPDX许可证列表（称为SPDX许可证ID）的简短标识符的SPDX许可证标识符语法，可以在任何级别（从包到源代码文件级别）指示相关的许可证信息。在单行注释中使用“SPDX许可证标识符”短语和由SPDX许可证ID组成的许可证表达式[29]，形成了一种精确、简洁且与语言无关的方式来记录许可证信息，这种信息易于机器处理。这使得源代码更易于阅读，这对开发者来说很有吸引力，并且可以通过grep轻松搜索许可证信息，并且许可证信息会随源代码一起传递。

要在项目的源代码中使用SPDX许可证ID，只需添加以下格式的单行代码，根据你的许可证和该文件语言的注释风格进行调整。例如：

>// SPDX-License-Identifier: MIT <br>
>/* SPDX-License-Identifier: MIT OR Apache-2.0 */ <br>
># SPDX-License-Identifier: GPL-2.0-or-later <br>

要了解如何在源代码中使用SPDX许可证ID，请参阅SPDX项目[30]的文档以及David Wheeler的教程[31]。

这些短标识符用于识别许可证的做法已被其他上游开源项目和代码库采纳，包括GitHub的许可证应用程序接口（API）[32]。除了U-Boo，Linux正在过渡到使用SPDX许可证ID，而像Zephyr和Hyperledger Fabric这样的较新项目从一开始就将其作为最佳实践采纳[33]。事实上，为了获得核心基础设施倡议（Core Infrastructure Initiative）的金牌徽章，源代码中的每个文件都必须有许可证，推荐的方式是使用 SPDX许可证ID[34]。

>项目必须在每个源文件中包含一个许可证声明。这可以通过在每个文件开头附近的注释中包含以下内容来完成：SPDX-License-Identifier: [项目的SPDX许可证表达式]。

当使用SPDX许可证ID时，收集项目文件中的许可证信息开始变得像运行“grep”一样简单。如果一个源文件在不同的包中被重用，许可证信息会随源代码一起移动，降低了许可证识别错误的风险，并使接收项目的许可证合规变得更容易。通过在许可证表达式中使用SPDX许可证ID，许可证组合的含义可以被更准确地理解。“这个文件是MPL/MIT”这种说法是模糊的，让接收者不清楚他们的合规要求。而“MPL-2.0 AND MIT”或“MPL-2.0 OR MIT”这种说法则明确指出了当重新分发文件时，许可证持有者必须遵守两个许可证，还是任何一个许可证。

正如Linux内核正在进行的转变所展示的那样，35 SPDX许可证ID可以逐步采用。你可以从为新文件添加SPDX许可证ID开始，而不改变代码库中已经存在的任何内容。

## 7.4 为开发者提供便利的工具和最佳实践

2017年，自由软件基金会欧洲（FSFE）创建了一个名为REUSE.software的项目，为开源项目提供了如何将SPDX许可证标识符应用于项目中的指导。同年晚些时候，REUSE.software指南被用于将SPDX许可证标识符添加到Linux内核中[37]。除了REUSE.software项目提出的指南外，还有一种linter工具，如果遵循这些指南，它可以自动生成软件物料清单（SBOM）。[38]

SPDX项目还维护了一系列基于Java[39]、Python[40]和Go[41]的工具，以帮助验证SPDX文档并在支持的文件类型之间进行转换。这些库可供其他工具创建者使用，可以简化他们使用软件创建和使用SPDX文档的过程。与规范和许可证列表一样，对于SPDX工具的改进建议也表示欢迎。

## 7.5 SPDX文档的采用情况

在过去的六年中，SPDX作为一种被认可的软件物料清单（SBOM）格式的使用逐渐得到了改善。一个关键因素是开源扫描工具FOSSology[42]在2016年能够输出SPDX文档，然后在2019年增加了使用这些文档的能力。在此之前，唯一能够处理该格式的扫描工具是专有的。此后，其他开源工具也开始可用于帮助特定工作流程，这正在加速SPDX的采用。

另一个关键因素是OpenChain规范的日益采用，该规范在其符合性标准中要求能够为软件创建物料清单（BOM）。[43] 随着（ISO/IEC 5230:2020）的讨论，如第6章所述，供应链中的采购部门更有可能期望在软件交付时看到一个SBOM，而生产这个SBOM将变得更加简单。SPDX规范在2021年成为国际标准（ISO/IEC 5962:2021）。

2018年6月，国家电信和信息管理局（NTIA）与多个行业的利益相关者展开了关于软件透明度的讨论，旨在确定最低可行的SBOM是什么，以及哪些文件格式可以支持这些信息。在2019年第一阶段工作总结时[44]，SPDX被认可为支持SBOM的有效标准。第二阶段使用SPDX在医疗设备制造商与卫生交付组织之间共享数据。

近年来，业界对提高运行在系统上的软件透明度的需求有了越来越强的认识。2020年11月，欧盟发布了《网络信息安全局（ENISA）报告》，《物联网安全指南》，该报告指出，提供物联网（IoT）设备的SBOM作为最佳实践[45]。2021年5月12日，美国拜登政府发布了网络安全行政命令，要求在增强软件供应链安全（第4节）中“为每个产品提供软件物料清单（SBOM），直接向购买者提供或通过公开网站发布”（第4节（e）（vii））。

## 7.6 未来发展方向

SPDX社区正在考虑如何最好地在规范的下一个版本中表示几个用例。从NTIA SBOM框架工作组47的努力以及OpenChain日本团队创建SPDX Lite配置文件[48]的努力中，人们清楚地认识到需要有一个最小的基础字段集来仅表示清单和关系。SPDX社区正在努力将规范重构为一个基本的字段集（称为核心）以及可选的配置文件，以处理特定领域的信息，如许可证、安全、血统、来源和使用情况。如果你在这些领域有使用案例，并且想参与讨论，请在GitHub上提出问题，或者发送邮件至spdx-tech@lists.spdx.org。

## 参考文献

>[1] <https://spdx.dev> accessed 22 June 2022. <br>
>[2] <https://spdx.github.io/spdx-spec/> accessed 22 June 2022. <br>
>[3] https://ntia.gov/report/2021/minimum-elements-software-bill-materials-sbom 访问于22日 <br>
>[4] https://spdx.github.io/spdx-spec/RDF-object-model-and-identifier-syntax/ 访问于2022年6月22日。<br>
>[5] https://spdx.org/rdf/terms/ 访问于2022年6月22日。<br>
>[6] https://spdx.org/licenses/ 访问于2022年4月14日。<br>
>[7] https://spdx.org/licenses/CC0-1.0.html 访问于2022年6月22日。<br>
>[8] https://spdx.github.io/spdx-spec/document-creation-information/ 访问于2022年6月22日。<br>
>[9] https://github.com/zephyrproject-rtos/zephyr 访问于2022年6月22日。<br>
>[10] https://github.com/zephyrproject-rtos/zephyr/blob/main/LICENSE 访问于2022年6月22日。<br>
>[11] https://spdx.github.io/spdx-spec/package-information/ 访问于2022年6月22日。<br>
>[12] https://spdx.github.io/spdx-spec/file-information/ 访问日期：2022年6月22日。<br>
>[13] https://spdx.github.io/spdx-spec/snippet-information/ 访问日期：2022年6月22日。<br>
>[14] https://spdx.org/licenses/ 访问于2022年6月22日。 <br>
>[15] https://spdx.github.io/spdx-spec/other-licensing-information-detected/ 访问于2022年6月22日。<br>
>[16] https://github.com/spdx/spdx-spec/issues 访问日期：2022年6月22日。<br>
>[17] https://spdx.github.io/spdx-spec/relationships-between-SPDX-elements/ 访问日期：2022年6月22日。<br>
>[18] https://spdx.github.io/spdx-spec/annotations/ 访问日期：2022年6月22日。 <br>
>[19] https://github.com/spdx/spdx-spec/issues 访问日期：2022年6月22日。<br>
>[20] https://www.iso.org/standard/81870.html 访问日期：2022年6月22日。
>[21] https://spdx.dev/ 访问日期：2022年6月22日。<br>
>[22] https://spdx.github.io/spdx-spec/ 访问日期：2022年6月22日。<br>
>[23] https://spdx.org/licenses/ 访问日期：2022年6月22日。<br>
>[24] https://spdx.org/spdx-spec/ 访问日期：2022年6月22日。<br>
>[25] https://github.com/spdx/license-list-data 访问日期：2022年6月22日。<br>
>[26] https://spdx.github.io/spdx-spec/ 访问日期：2022年6月22日。<br>
>[27] https://git.denx.de/?p=u-boot.git;a=commit;h=eca3aeb352c964bdb28b8e191d6326370245e 访问日期：2022年6月22日。<br>
>[28] https30 https://spdx.dev/ids-how 访问日期：2022年6月22日。
>[29] https://spdx.dev/ids 访问日期：2022年4月14日。<br>
>[30] https://spdx.dev/ids-how 访问日期：2022年6月22日。<br>
>[31] https://github.com/david-a-wheeler/spdx-tutorial 访问日期：2022年6月22日。<br>
>[32] https://developer.github.com/v3/licenses/ 访问日期：2022年6月22日。<br>
>[33] https://spdx.dev/ids-where 访问日期：2022年6月22日。<br>
>[34] https://github.com/coreinfrastructure/best-practices-badge/blob/master/doc/other.md#basics-1 访问日期：2022年6月22日。<br>
>[35] https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/proc 访问日期：2022年6月22日。<br>
>[36] https://reuse.software/ 访问日期：2022年6月22日。<br>
>[37] https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/process/license-rules.rst 访问日期：2022年6月22日。<br>
>[38] https://github.com/fsfe/reuse-tool 访问日期：2022年6月22日。<br>
>[39] https://github.com/spdx/tools/ 访问日期：2022年6月22日。<br>
>[40] https://github.com/spdx/tools-python 访问日期：2022年6月22日。<br>
>[41] https://github.com/spdx/tools-golang 访问日期：2022年6月22日。<br>
>[42] https://www.fossology.org/ 访问日期：2022年6月22日。<br>
>[43} https://wiki.linuxfoundation.org/_media/openchain/openchainspec-2.0.pdf 访问日期：2022年6月22日。<br>
>[44] https://www.ntia.gov/SBOM 访问日期：2022年6月22日。<br>
>[45] https://www.enisa.europa.eu/publications/guidelines-for-securing-the-internet-of-things 访问日期：2022年4月14日。<br>
>[46] https://www.federalregister.gov/documents/2021/05/17/2021-10460/improving-the-nations-cybersecurity 访问日期：2022年6月22日。<br>
>[47] https://www.ntia.gov/SBOM 访问日期：2022年6月22日。<br>
>[48] https://spdx.github.io/spdx-spec/SPDX-Lite/ 访问日期：2022年6月22日。<br>
