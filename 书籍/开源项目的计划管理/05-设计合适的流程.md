作为一个计划经理，流程是你的心病。你可能不喜欢它们，但你承认它们的价值。一个精心设计的流程可以提高工作的可见度，帮助贡献者之间的协调，并为所有参与者提供一个一致的、可预知的经验。你会花很多时间参与流程，要么直接参与，要么作为顾问为那些正在研究自己流程的团队服务。在这一章中，你将学习如何设计一个适合你的社区需要的流程，而又不会成为太大的负担。
有些人为了流程而享受流程。在一个经常感到失控的世界里，它给他们一种控制感。但总的来说，开源社区的贡献者并不是这些人。特别是如果他们是自愿付出时间的话，贡献者不希望这感觉像工作。他们想做很酷、很有趣的开源事情。

这并不是说，你的社区不会遵循任何流程。他们明白，酷的、有趣的事情需要遵循一定的流程--即使是Calvinball也有一个规则。你只需要证明这个过程使生活更美好。要做到这一点，它必须解决一个问题。让我们从这里开始。

# 1. 界定背景

**问题：你对这个过程要解决什么问题缺乏明确的认识。**

就像做决定一样，你不需要对问题的存在达成一致意见，尽管如果每个人都能认识到有人在经历这个问题，那会有帮助。如果没有问题需要解决，你就不需要创建一个过程。如果你牢记这一点，你就不太可能仅仅因为觉得有义务建立一个过程而建立一个过程。

## 设定目标

界定背景的第一步是设定过程的目标。你要陈述问题，但要用一种快乐的方式。例如，在创建一个错误分流过程中，你的目标可能是将解决问题的平均时间减少10%。更有可能的是，你不会那么具体，而是将目标设定为减少解决问题的平均时间。

有些目标是可以衡量的，就像我们在前一段的例子。但这并不意味着你在设定目标时需要具体化。重要的部分是变化的方向，而不是幅度。这很好，因为你可能为之建立一个过程的许多目标都很难衡量。例如，正如你将在《管理功能》中读到的，功能规划过程的目标之一是改善协调。你如何衡量这个目标？一般来说，你可以通过 "感觉 "知道你是否成功。
一个过程可以而且经常会有多个目标。有时，这些目标似乎是不相关的。回到错误分流的例子，目标可以包括缩短解决问题的时间，提高开发人员的效率，以及发展贡献者社区。此外，几个过程可以为同一个目标服务。壮大贡献者社区的目标可以成为错误分流过程和礼品赠送过程的一个激励因素。

你为流程设定的目标回答了 "我们为什么要创建一个流程？" 几乎任何你对这个问题的回答都是正确的，只要它能满足你的社区的需要。("因为我们的同行项目有一个这样的过程"，这本身就是一个错误的答案。) 重要的是要清楚地知道目标是什么。这有助于你获得社区的支持，并帮助你在设计过程中保持目标。

## 界定限制条件

任何一个孩子的父母都知道，仅仅想要一件东西是不够的。你可以想在月球上骑一匹小马，但这并不意味着它可以真实地发生。开放源码的贡献者不是孩子，但你仍然可以发现社区希望得到一些不可能实际发生的东西。当设计一个流程时，你必须把自己限制在可能的范围内。这意味着要确定你的约束条件。

你的一些限制因素将是技术的限制。技术的限制是在可能领域之外的东西。这些限制可能是普遍存在的--你永远无法让你的网络数据包比光速更快。还有一些可能只适用于你所做的特定技术选择--无论你多么希望它不是这样，Subversion都将是集中式的。在后一种情况下，你可能有一个合理的理由来选择限制你，但它们仍然限制着你。你可能会决定做一些改变来消除这个限制，但对于你现在设计的流程来说，这个限制是存在的。

其他限制是原则性的限制。这些东西在技术上是可行的，但对你的社区来说是不可接受的。例如，一个专有的错误跟踪器可能满足你的所有需求，但你的社区承诺只使用开放源码软件。或者你可能不得不使用一个开源错误跟踪器的自我托管版本，因为社区不会接受在其直接控制之外的软件即服务版本。并非所有的原则约束都是技术性的。你的社区可能要求所有贡献者同意遵守行为准则或签署贡献者许可协议。对原则问题的看法在不同的开放源码社区之间有很大的不同，而且往往是在开放源码社区内部，所以你要确定哪些适用于你的特定社区，这一点非常重要。

现在我们已经确定了可能领域的界限，现在是时候弄清楚什么是可能的了。资源的限制在任何社区驱动的项目中都是常见的。(事实上，它们在任何项目中都很常见。你在日常工作中听到过多少次 "我们没有这个预算"？） 资源限制往往表现为硬件限制。你没有足够的磁盘空间来保存每一个构建，所以你必须在一定时间后清理旧的构建。或者你不能为大型计算机建立你的应用程序，因为你没有一个大型计算机可供建立。也许你不能举行年度贡献者会议，因为你没有资金为每个人支付旅行费用。如果你没有现金限制，通常可以通过花钱来解决资源限制问题。而你很可能是。

最后两种类型的限制是密切相关的：方便的限制和时间的限制。一个耗时的过程往往是，但不总是，不方便的。但是，由于你面对的是一个主要由志愿者组成的社区，你对不便和时间的预算都很紧张。如果一个过程太不方便或太耗时，你的贡献者将更有可能找到另一个社区来参与其中。即使时间是 "隐藏的"，一个耗时过长的过程也不会有什么用处。如果你的博客编辑过程需要一个月的时间，那么你的项目对当下紧迫问题的评论在被阅读之前就已经过时了。每个过程都有某种时间限制：在过程的结果不再有用的时候，总会有一些上限。

# 2. 建立一个流程

**问题：你需要设计一个流程来适应你所确定的问题。**

现在你已经设定了你的目标并确定了约束条件，现在是建立流程的时候了。你需要考虑谁做什么，什么时候做。考虑信息和工件如何在流程中流动。是什么触发了各个步骤？某些条件是如何改变路径的？

## 寻找现有的模式

好消息! 你可能不需要从头开始发明一个流程。在开放源码中，我们经常从其他社区借用想法。有一个很好的机会，别人已经有了一个做你所追求的事情的过程。就从这个开始吧。如果有必要，你可以调整它以满足你的社区的具体需要。

但也许你想做的是其他项目没有做过的事情。(在这种情况下，你仍然不一定要从头开始。你可以从你的贡献者已经有的行为中寻找模式。哪些规范是有机地建立起来的？例如，人们在开始编码新功能之前是否已经分享了设计文件？如果是这样，那么将其作为一个步骤纳入功能流程是有意义的。在最好的情况下，流程将现有的规范编成法典，以确保它们被可靠地遵循。

想一想道路。一些道路从一开始就作为发展的一部分被规划。但是，其他的道路开始时是由旅行者--无论是人类还是动物--重复地沿着类似的路线走过的。这些道路并不总是沿着一条直线前进，但它们体现了价值数千次反复的知识。随着时间的推移，旅行者了解到哪里的地形最容易，这就成了道路。你可以通过记录现有的实践，以同样的方式建立一个流程。

## 绘制流程草图

无论你是从头开始还是借用现有的流程，下一步都是勾勒流程。一个 "草图 "不一定是一张真正的图画--尽管那些图画会有帮助，你一会儿就会看到--书面描述也可以。事实上，先把它写出来可以帮助你画图，所以我们先来谈谈这个。

写下过程的流程并不是炫耀你的文学才华的地方。事实上，你的写作越接近小学，就越好。写出简单的句子，即简单的主语、动词和宾语。把步骤放在一个有序的列表中，这样你就可以在需要时参考以前的步骤。如果这个过程有条件，从开始到结束的一个路径开始。然后再加入其他的路径。下面的例子显示了你如何勾勒出一个功能建议的过程。

1. 功能拥有者填写提案模板。
2. 特征拥有者向特征管理人提交建议。
3. 功能管理者检查提案：
- 3a. 如果提案是完整的，继续下一步。
- 3b. 如果提案不完整，则返回到#1。
4. 功能管理者向社区宣布提案。
5. 功能管理者向技术委员会提交建议。
6. 技术委员会对提案进行投票：
- 6a. 如果通过，继续下一步。
- 6b. 如果被拒绝，过程结束。
7. 功能拥有者实施建议。

你可能已经注意到，这个例子并没有说到步骤是如何发生的。功能所有者在哪里填写提案模板？他们如何提交给功能管理人？在宣布提案后，功能管理人要等多久才能将其提交给技术委员会？这些都是需要回答的问题，但不是马上就能回答。一旦你建立了流程的骨架，你就可以加入这些细节了。

现在你已经有了一个流程的粗略草图，你可以做实际的草图（如果你的绘画技巧不是很好，也可以做数字图片）。在试图表达这个过程时，有两种图很方便：泳道图和流程图。

泳道图看起来有点像一个游泳池，因此被称为泳道图。它对于吸引人们注意谁执行流程中的哪些步骤很有用。每条泳道代表一个人或一个团队，每个步骤出现在相应的泳道上。第48页上的泳道图说明了前面讨论的功能建议的流程。

![输入图片说明](../../%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/Program%20Management%20for%20OSS-04.png)

流程图是你可能已经熟悉的东西。它代表了流程中从一个步骤到另一个步骤的运动，但没有强调谁来执行这个步骤。(严格来说，泳道图也是流程图的一种。)由于行为者没有被强调(或完全被忽略)，流程图对于表现对象的角度很有用。例如，下面的流程图显示了一个功能建议是如何通过前面描述的过程进行的。

![输入图片说明](../../%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/Program%20Management%20for%20OSS-05.png)

##平衡利弊

如你所知，项目管理的一个重要部分是找到正确的平衡。当你设计一个流程时，你必须平衡不同选项之间的权衡。在一个理想的世界里，你可能会设计出一个资源无限、人人都爱你的过程，而在现实世界里，你几乎没有任何硬件或资金可以投入到这个问题上，而且参与者往往对这个过程不感兴趣。

在你平衡这些权衡的时候，你需要比较收益和成本。但你不能只做一个直接的成本效益分析。通常在商业世界中进行的成本效益分析，是在整个组织中看成本和效益的总量。这在一个开源社区是行不通的。首先，你不可能把所有的东西都转换成美元数额。其次，更重要的是，开源社区不像工作那样有 "粘性"，有人会因为你的参与而给你一份薪水。贡献者当然关心社区的整体健康，但如果一个过程对社区来说是一个净利益，但对他们来说是一个净损害，他们就不可能忍受它。

这里的成本效益分析并不是将正负数字相加，得到一个单一的答案。相反，它是一个看成本和效益是什么以及它们适用于谁的问题。这使你在选择时能够有意识地进行。设计一个过程，使一些参与者比其他参与者受益更多，这是可以的--事实上，大多数过程都是这样运作的。但你应该意识到这种选择，这样你就可以在整体上努力做到公平。

平衡成本和收益发生在整个过程中，但拥有上一节中的图表可以帮助指出存在不适当成本的地方。例如，如果你的泳道图显示一个人在一个过程中行动了三次，你可以看看这些步骤是否可以连续放置。这样一来，这个人就可以一气呵成地完成所有的动作，然后把结果交给下一个人。一般来说，图表越复杂，所需费用就越多。(不幸的是，相反的情况并不成立。一个简单的图仍然可以有昂贵的步骤）。

现在你已经对你要平衡的权衡有了全面的了解，回到你的流程骨架上，开始填写细节。包括人们在哪里或如何执行他们的步骤。注意步骤之间的依赖性，如果合适的话，加入等待期。

# 3. 实施流程

**问题：你需要将流程付诸行动。**

祝贺你设计了一个宏伟的流程。你通过从现有的模式中勾勒出最初的草案，平衡权衡，并填入细节，建立了一个流程。现在是时候让它走向世界了。但是，等等! 在你完全实施这个流程之前，你要对它进行一次测试。这本质上是用户接受度测试，但是是针对流程而不是软件。(当然，你的流程可能涉及新的软件，所以有时也是软件的用户接受度测试）。

为了测试这个过程，你要找几个人给它提供真实的使用。作为设计者，你离这个过程太近了，无法公平地评估它。另外，你可能不是唯一会参与这个过程的人，所以你可能没有以不同角色参与的人的观点。你想找一个在社区有经验的人，特别是在他们会使用这个过程的背景下。(例如，一个长期的文档贡献者，如果没有参加过发布工程活动，可能不会给你关于发布工程流程的最佳反馈。） 他们应该能够给这个过程一个公平的评价。你不希望有一个对流程有敌意的人去测试它，但你确实希望有人能告诉你哪些部分不工作。

> 测试破坏了项目，但拯救了世界
>
> Fedora 的变更流程（我们在管理功能中描述的流程版本）使用维基页面作为数据存储。它涉及到大量的复制和粘贴，以及天真地尝试在脚本中抓取和解析这些页面。这很容易出错，也很不方便（至少对我来说），所以我决定尝试一些新的东西。我建立了一个看板，每张卡片上都有大量的自定义字段，以使建议更容易被解析。在一个夏天的时间里，我指导一个学生编写自动化程序来处理每一个步骤的整理工作。我们摆脱了复制、粘贴和解析基本上是自由格式的文本。每一步，提交后都是通过API调用，并有明确的数据限制。这是一个巨大的改进。
>
> 然后，我请一位值得信赖的、有经验的社区成员对它进行了几次测试运行。这时我们发现，我们的工作所基于的工具要求每个自定义字段都要单独保存。有20个左右的独立字段，每个字段都需要点击保存按钮，否则输入的内容就会丢失。我认为这对将要提交提案的开发人员来说太不方便了。在每个字段上点击保存按钮是很快的，很多人都会习惯性地这样做，但对于那些没有这样做的人来说，失去工作的不便就太大了。我们最终决定保持现状。
>
> 我很高兴我们在向全社会推广新程序之前进行了测试。实习生和我在很大程度上使用了测试数据，并且更专注于胶水脚本的工作方式。我们并没有太注意我们所使用的工具。让别人用真实世界的使用情况进行测试，突出了我们没有注意到的缺陷。

如果你的真实世界测试带来了缺陷，就回去修正它们。一旦真实世界的测试看起来不错，现在是时候让这个过程真正到位了。要做到这一点，你必须要有文件。

你开发的图表是有用的，但它们是不够的。你还需要写出这个过程。包括与工具、支持性文件的链接，等等。流程的一些文件 "规则 "是：步骤的标准，如何处理例外情况，以及如果流程被跳过，有什么补救措施。

为了后来参与的人，你应该记录流程中各步骤背后的理由。解释权衡有助于未来的你避免以后做出错误的权衡。为了保持流程文件的清晰，不要把 "为什么？"文件放在 "什么/怎么做？"文件的中间。它是为那些有兴趣的人提供的历史参考，并且会分散那些试图弄清楚如何参与这个过程的读者的注意力。

现在你已经准备好了你的文档，你准备向社区宣布这个过程。作为开放源码，你可能一直在工作，即使不是公开的，至少也不是一直在隐藏。但那些没有直接参与创建新流程的人在很大程度上忽略了这项工作。所以现在是你介绍它的时候了。分享你写的文档。解释它的好处。在反对意见表达之前，预测并反驳它们。你是在向社区推销新的流程，所以要拿出所有的东西来。

# 4. 进行修订

**问题：流程已经偏离了社区的需求。**

如果你很幸运，你的新流程在你推出的那一天是完美的。它符合所有的目标，每个人都喜欢它。但是，即使是一个完美的流程也会随着时间的推移而变得不完美。你周围的世界不断变化。
技术的转变，改变了人们的喜好和可能。SourceForge曾经是托管软件发布和版本控制的地方。现在，GitHub是许多开发者的默认选择。在许多社区，邮件列表正在被Discourse所取代，互联网中继聊天正在被放弃，以支持更丰富的聊天协议。今天对你的社区有用的东西，几年后可能就会变得非常陈旧。

那么，你怎么知道什么时候该做改变？毕竟，你不想 "修复 "没有坏掉的东西。最简单的方法是寻找痛点。你是否看到人们在抱怨过程中的特定步骤？也许它们需要被改变。是否有该流程旨在防止的意外发生？也许它需要被调整或扩展。例如，如果新的贡献者没有注册邮件列表，那么随着时间的推移，越来越少的人会收到这些信息。

除了痛苦的哀号之外，一个需要修改的过程的另一个迹象是沉默。在有可能的情况下，人们会直接跳过他们认为没有价值的过程。如果你注意到社区成员正在跳过全部或部分过程，你有两个选择。第一个是推销该过程的好处。也许他们并不欣赏这个过程对他们和社区的作用。另外，你可以把它当作一个迹象，表明这个过程并没有增加预期的价值。两者都可能是真的，但如果有很多人跳过这个过程，那就表明可能的答案是后者。记住，程序的存在是为了服务于社区；社区的存在不是为了服务于程序。

一旦你确定你需要修改这个过程，你就再次经历本章的过程。这一次，你所研究的现有模式就是当前的过程。但不要以为目标和限制因素仍然是一样的。如果它们发生了变化，建立在它们之上的过程也必须改变。

# 5. 回顾

像任何技能一样，卓越的流程设计需要经验。如果你采取迭代的方法，你将有很多机会来完善你早期的决定。但是，如果你一开始就清楚地定义了问题和约束条件，在权衡中做出有意的选择，并清楚地传达了价值，你就会有一个好的开始。社区可能不喜欢这个过程，但他们会看到这个过程的好处，并会心甘情愿地跟随。
