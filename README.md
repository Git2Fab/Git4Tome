# Git4Tome-用开源精神传播开源知识

## 介绍

Git4Tome是一个将开源领域的经典书籍、论文、文献和报告翻译成中文的兴趣项目。通过翻译这些经典书籍，我们希望能够为中国的开源社区提供更多高质量的学习和参考资源，帮助开发者、技术人员以及开源爱好者更好地理解和应用开源理念，将开源的思想、方法和理论融入到日常的工作中。

**让我们一起，用代码和文字，架起开源世界的桥梁！**

## 为什么要翻译？

1. 从现象到本质：开源软件和工具的广泛使用确实为开发者提供了强大的工具和平台，但若只停留在使用层面，而不深入了解其背后的运作机制、法律框架、社区治理等，就如同只知其然，而不知其所以然。翻译工作能够帮助开发者深入理解开源的哲学和原则，从而在实践中更好地运用这些工具和理念，甚至参与到开源社区的建设和发展中。
2. 塑造知识体系：翻译工作不仅是语言的转换，更是知识的传递和积累。通过系统地翻译开源领域的经典著作和文档，可以构建起一个完整的开源知识体系，为中文用户提供从入门到精通的学习路径。这样的知识体系对于培养开源人才、推动开源项目的发展具有重要意义。
3. 融合文化差异：开源文化在全球范围内虽有共通之处，但在不同地区也会展现出文化特色。通过翻译，可以将国际开源社区的最佳实践与本地文化相结合，使之更易于被本地社区接受和实践。这不仅有助于开源文化的本土化，还能促进全球开源社区的多样性和包容性。
4. 激发开源创新：开源的核心在于协作和共享，而创新往往源于对现有知识的深入理解和重新组合。通过翻译工作，将国际上的先进开源理念和实践引入中文社区，可以激发开发者的新思路，促进技术创新和商业模式的创新。同时，这也有助于中国开发者在全球开源舞台上发出自己的声音，贡献自己的智慧。
5. 促进国际交流：翻译工作不仅是单向的知识传播，还可以成为双向交流的平台。通过翻译，我们可以将中国开源社区的独特见解和创新成果传播到国际社区，促进全球开源生态系统的多元化发展。

通过翻译，开源不仅是技术的共享，更是文化和思想的碰撞与交融。这一过程不仅推动了开源知识的传播，也促进了技术的普及与创新，塑造了更具全球视野的开源认知。

## 如何贡献

1. 查看翻译项目：浏览当前正在进行的翻译项目，选择您感兴趣的书籍。
2. Fork 项目并提交 PR：Fork 项目到您的个人仓库，进行翻译或校对，然后通过 Pull Request 提交您的贡献。
3. 反馈与讨论：参与讨论和反馈，为翻译项目提供改进意见，确保内容的准确性和流畅性。

## 书籍目录

||书名|作者|
|---|---|---|
|01|[《开源项目的计划管理》<br>（Program Management for Open Source Projects）](./书籍/开源项目的计划管理/README.md)|本·科顿<br>Ben Cotton|
|02|[《道路与桥梁：数字基础设施背后的隐形劳动力》<br>（Roads and Bridges: The Unseen Labor Behind Our Digital Infrastructure)](./书籍/道路与桥梁/README.md)|纳迪娅·阿斯帕鲁霍娃/<br>Nadia Asparouhova|
|03|[《开放协作：开源软件的创造与维护》<br>（Working in Public: The Making and Maintenance of Open Source Software）](./书籍/开放协作/README.md)|纳迪娅·阿斯帕鲁霍娃<br>Nadia Asparouhova|
|04|[《开源法律、政策与实践》<br>（Open Source Law, Policy and Practice）](./书籍/开源法律、政策与实践/README.md)|阿曼达·布洛克<br>Amanda Brock|

## 研究报告目录

||报告|作者|
|---|---|---|
|01|[《开源软件的价值》<br>（The Value of Open Source Software）](./研究报告/开源与经济/开源软件的价值.md)|Hoffmann, Manuel, Frank Nagle, and Yanuo Zhou<br>Harvard Business School|
|02|[《利用开源构建业务》<br>（Building a Business on Open Source）](./研究报告/开源与经济/利用开源构建业务.md)|ObjectBox|
|03|[《以稳定的基金会发展开源项目》<br>(Growing Open Source Projects with a Stable Foundation)](./研究报告/开源与经济/以稳定的基金会发展开源项目.md)|Martin Michlmayr|

## 指南手册

||指南|作者|
|---|---|---|
|01|[《开源之道指南手册2.0》<br> (The Open Source Way GuideBook 2.0)](./指南手册/开源之道指南手册2.0/README.md)|[开源之道贡献者](https://github.com/theopensourceway/guidebook/blob/main/contributors.adoc)|
|02|[《The TODO Group OSPO101培训课程》<br>(The TODO Group OSPO 101 Training Modules)](./指南手册/OSPO101培训课程/README.md)|[The TODO Group](https://todogroup.org/)|

## 版权说明

- 原著作品的版权归原作者所有
- 翻译作品采用 CC-BY-NC-SA 4.0 许可证
- 翻译者保留其翻译作品的版权
- 翻译内容仅供学习交流使用，不得用于商业目的
- 建议读者购买正版图书支持原作者
- 若原作者或出版社认为侵犯了版权，请联系我们进行处理

## 核心贡献&维护者

|姓名|邮件|
|---|---|
|野行僧郭晧|[guohao@gitconomy.cn](mailto:guohao@gitconomy.cn)|
