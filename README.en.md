# Git4Tome - Spreading Open Source Knowledge with the Open Source Spirit

## Introduction

**Git4Tome** is an interest-driven project that translates classic books, papers, documents, and reports from the open-source field into Chinese. Through the translation of these classic works, we aim to provide the Chinese open-source community with more high-quality learning and reference resources, helping developers, technical professionals, and open-source enthusiasts better understand and apply open-source principles. Our goal is to integrate open-source thinking, methods, and theories into everyday work.

Together, let's build bridges across the open-source world through code and words!"

## Why Translate?

1. **From Phenomenon to Essence**  
   The widespread use of open-source software and tools provides developers with powerful platforms, but staying at the level of mere usage without delving into their underlying mechanisms, legal frameworks, and community governance is akin to knowing the "what" without understanding the "why." Translation helps developers grasp the philosophy and principles behind open source, enabling them to apply these tools and ideas more effectively in practice and even contribute to the growth and development of open-source communities.  

2. **Building a Knowledge System**  
   Translation is not just about converting language but also about transmitting and accumulating knowledge. Systematically translating classic works and documentation in the open-source field can build a comprehensive knowledge system, offering Chinese users a clear learning path from beginner to advanced levels. This system is crucial for cultivating open-source talent and driving the development of open-source projects.  

3. **Bridging Cultural Differences**  
   While open-source culture has universal aspects, it also reflects cultural characteristics in different regions. Translation allows the integration of global best practices with local culture, making them more accessible and practical for local communities. This facilitates the localization of open-source culture and promotes diversity and inclusiveness within the global open-source ecosystem.  

4. **Fostering Open-Source Innovation**  
   The core of open source lies in collaboration and sharing, and innovation often stems from a deep understanding and recombination of existing knowledge. Translation introduces advanced international open-source ideas and practices to Chinese communities, inspiring developers with new perspectives and fostering technological and business model innovation. Additionally, it empowers Chinese developers to contribute their voices and expertise to the global open-source stage.  

5. **Promoting International Exchange**  
   Translation is not just a one-way dissemination of knowledge but also a platform for mutual exchange. By translating, we can share the unique insights and innovative achievements of the Chinese open-source community with the global audience, enriching the diversity of the global open-source ecosystem.  

Through translation, open source becomes more than just the sharing of technology; it becomes a collision and fusion of cultures and ideas. This process not only facilitates the dissemination of open-source knowledge but also promotes technological adoption and innovation, shaping a more globally informed understanding of open source.  

## Books Index

||Book|Author|
|---|---|---|
|01|[Program Management for Open Source Projects](./书籍/开源项目的计划管理/README.md)|Ben Cotton|
|02|[Roads and Bridges: The Unseen Labor Behind Our Digital Infrastructure](./书籍/道路与桥梁/README.md)|Nadia Asparouhova|
|03|[Working in Public: The Making and Maintenance of Open Source Software](./书籍/开放协作/README.md)|Nadia Asparouhova|
|04|[Open Source Law, Policy and Practice](./书籍/开源法律、政策与实践/README.md)|Amanda Brock|

## Research Index

||Reprots|Author|
|---|---|---|
|01|[The Value of Open Source Software](./研究报告/开源与经济/开源软件的价值.md)|Hoffmann, Manuel, Frank Nagle, and Yanuo Zhou<br>Harvard Business School|
|02|[Building a Business on Open Source）](./研究报告/开源与经济/利用开源构建业务.md)|ObjectBox|
|03|[Growing Open Source Projects with a Stable Foundation](./研究报告/开源与经济/以稳定的基金会发展开源项目.md)|Martin Michlmayr|

## GuideBook Index

||GuideBooks|Author|
|---|---|---|
|01|[The Open Source Way GuideBook 2.0](./指南手册/开源之道指南手册2.0/README.md)|[开源之道贡献者](https://github.com/theopensourceway/guidebook/blob/main/contributors.adoc)|
|02|[The TODO Group OSPO 101 Training Modules](./指南手册/OSPO101培训课程/README.md)|[The TODO Group](https://todogroup.org/)|

## How to Contribute

1. **Browse Translation Projects**: Check out the ongoing translation projects and choose the book that interests you.
2. **Fork the Project and Submit a PR**: Fork the project to your personal repository, perform translations or proofreading, and submit your contributions via a Pull Request.
3. **Feedback and Discussion**: Participate in discussions and provide feedback to improve the translation project, ensuring content accuracy and fluency.

## Copyright Notice

- The copyright of the original work belongs to the original author.
- Translated works are licensed under the [CC-BY-NC-SA 4.0 License](https://creativecommons.org/licenses/by-nc-sa/4.0/).
- Translators retain the copyright of their translated works.
- Translated content is for educational and exchange purposes only and cannot be used for commercial purposes.
- We encourage readers to purchase the original books to support the authors.
- If the original author or publisher believes that copyright has been infringed, please contact us for resolution.

## Core Contributors & Maintainers

|Name|Email|
|---|---|
|Guo Hao (Nomadic Master)|[guohao@gitconomy.cn](mailto:guohao@gitconomy.cn)|
